FROM python:3.7-slim

COPY ./ .

RUN pip install --no-cache-dir pipenv
RUN pipenv install
RUN pipenv run ./manage.py makemigrations plankton
RUN pipenv run ./manage.py migrate

ENTRYPOINT ["pipenv", "run", "./manage.py", "runserver", "0.0.0.0:8000"]
EXPOSE 8000