#!/usr/bin/python3

"""
Application django plankton
===========================

Cette application sert de point d'accès au ConvNet DeepFashion que nous avons conçu.

"""

from tensorflow.keras.applications import InceptionResNetV2
from h5py import File

ConvNet = InceptionResNetV2(input_shape=(299, 299, 3), weights="imagenet", include_top=False, pooling="max")


def extract_normalized_feature(b64_image: str):
    """

    Fonction d'extraction de caratéristique normalisé d'une image.

    :param: ``b64_image``, image transmise par requête ``POST`` sous format base 64.
    :return:
    """
    from tensorflow.keras.preprocessing.image import load_img, img_to_array
    from tensorflow.keras.applications.inception_resnet_v2 import preprocess_input
    from numpy import expand_dims, linalg
    from base64 import b64decode
    from re import sub
    from io import BytesIO

    preprocessed_image = preprocess_input(
        expand_dims(
            img_to_array(
                load_img(
                    BytesIO(
                        b64decode(sub("^data:image/.+;base64,", '', b64_image)),
                    ),
                    target_size=(299, 299, 4)
                )
            ),
            axis=0
        )
    )

    image_feature = ConvNet.predict(preprocessed_image)
    normalized_image_feature = image_feature[0] / linalg.norm(image_feature[0])
    return normalized_image_feature


def get_similar_image(normalized_image_feature):
    """

    Fonction de comparaison de caractéristiques, cette fonction permet de trier les images qui se ressemblent par le biais de leur caratéristiques.

    :param: ``normalized_image_feature``, caractéristique extraite d'une image par la fonction ``extract_normalized_feature``
    :return: ``imlist``, tuple contenant les chemins des 30 images les plus similaires
    """
    from numpy import dot, argsort

    try:
        with File("./features.h5", 'r') as f:
            features = f["dataset_feat"][:]
            img_names = f['dataset_name'][:]
            f.close()
    except OSError as e:
        print(f"Can't open features.h5 files : {e}")
        return None

    scores = dot(normalized_image_feature, features.T)
    rank_id = argsort(scores)[::-1]

    # number of top retrieved images to show
    maxres = 30
    imlist = (img_names[index].decode('utf-8') for i, index in enumerate(rank_id[0:maxres]))
    return imlist
