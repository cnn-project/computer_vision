from django.test import TestCase
from plankton.models import SearchModel, ImageModel


# Create your tests here.

class SearchTestCase(TestCase):
    def setUp(self):
        search = SearchModel.objects.create()
        self.search_id = search.id

    def test_search_exist(self):
        search = SearchModel.objects.get(id=self.search_id)
        assert search


class ImageTestCase(TestCase):
    def setUp(self):
        self.image = ImageModel.objects.create(
            path="https://i.pinimg.com/originals/11/ee/a6/11eea65aec214641c488a033d0b977cc.jpg")

    def test_image_have_path(self):
        assert self.image.path
