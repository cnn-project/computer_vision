from django.db import models


class ImageModel(models.Model):
    """

    La définition de l'image recherchée enregistrée dans la base de données

    """
    path = models.CharField(max_length=500, primary_key=True)

    def __str__(self):
        return self.path

    class Meta:
        verbose_name = "Image"
        verbose_name_plural = "Images"


class SearchModel(models.Model):
    """

    Modèle sauvegardé d'une recherche

    """
    images = models.ManyToManyField(ImageModel, through="SearchResultModel")

    def __str__(self):
        return self.images

    class Meta:
        verbose_name = "Search"
        verbose_name_plural = "Searches"


class SearchResultModel(models.Model):
    """

    Table de jointure entre les images en mémoire (dans la base de données) et la recherche associée

    """
    image = models.ForeignKey(ImageModel, on_delete=models.CASCADE)
    search = models.ForeignKey(SearchModel, on_delete=models.CASCADE)
