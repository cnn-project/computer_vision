from rest_framework import status
from rest_framework.reverse import reverse
from rest_framework.views import APIView
from rest_framework.response import Response
from plankton.models import SearchResultModel
from plankton.models import ImageModel
from plankton.models import SearchModel
from django.http import JsonResponse

from plankton.serializers import SearchSerializer


class SearchRequestViewSet(APIView):
    """

    :route: ``/search/``

    """

    def post(self, request):
        """
        Gestion des requêtes POST sur la route ``/search/``
        Cette méthode permet d'envoyer une recherche d'image similaire en passant l'image dans le corps de requête sous format base 64 en json.

        .. code-block:: console

            $ curl -X "POST" -H "Content-Type:application/json" -d @image.json localhost:8000/search/

        Notez ici l'utilisation de l'argument ``-d @image.json`` où le ``@`` fait référence au contenu du fichier ``image.json`` qui respecte le format suivant :

        .. code-block:: json

            {
              "image": "data:image/jpeg;base64,..."
            }

        :param: request: Instance de la requète ``HTTP``
        :return: JsonResponse
        """
        from plankton.__init__ import extract_normalized_feature
        from plankton.__init__ import get_similar_image
        if "image" in request.data:
            bytes_img = request.data["image"]
            detected_type = extract_normalized_feature(bytes_img)
            if not detected_type:
                return JsonResponse(
                    {"error": "can't extract image's feature", "status": status.HTTP_500_INTERNAL_SERVER_ERROR})
            similar_images = get_similar_image(detected_type)
            search = SearchModel.objects.create()
            for image in similar_images:
                image_instance, _ = ImageModel.objects.get_or_create(path=image)
                SearchResultModel.objects.create(image=image_instance, search=search)
            return JsonResponse({
                "id": search.id,
                "url": reverse("result", args=(search.id,), request=request),
                "status": status.HTTP_200_OK
            })


class ResultsViewSet(APIView):
    """

    :route: /results/

    """

    def get(self, request):
        """
        Gestion de requêtes GET sur la route ``/results/``.
        La méthode permet de récupérer la liste des recherches avec leurs "chemins".

        .. code-block:: console

            $ curl -H "Content-Type:application/json" localhost:8000/results

        :param: request
        :return: Response
        """
        searches = SearchModel.objects.all()
        return Response([reverse('result', args=(search.id,), request=request) for search in searches])

    def delete(self, request):
        """
        Gestion des requêtes DELETE sur la route ``/results/``.
        La méthode permet de supprimer toutes les recherches de la base de donnée

        .. code-block:: console

            $ curl -X "DELETE" -H "Content-Type:application/json" localhost:8000/results

        :param: request
        :return: Response
        """
        deleted, row_count = SearchModel.objects.all().delete()
        return Response({"deleted": row_count, "status": status.HTTP_200_OK})


class ResultViewSet(APIView):
    """
    :route: /results/(?P<pk>[0-9]+)/
    """

    def get(self, request, pk: int = 0, format=None):
        """
        Gestion des requêtes GET sur la route ``/results/(?P<pk>[0-9]+)/``.
        La méthode permet d'accéder une recherche en particulier en fonction de la clef primaire de la recherche.

        .. code-block:: console

            $ curl -H "Content-Type:application/json" localhost:8000/result/:pk

        :param: request
        :param: pk: primary key, référence l'id d'une recherche
        :param: format
        :return: Response
        """
        search = SearchModel.objects.get(id=pk)
        return Response({**SearchSerializer(search).data, "status": status.HTTP_200_OK})

    def delete(self, request, pk: int = 0, format=None):
        """
        Gestion des requêtes DELETE sur la route ``/results/(?P<pk>[0-9]+)/``.
        La méthode permet de supprimer une recherche en particulier en fonction de la clef primaire de la recherche.

        .. code-block:: console

            $ curl -X "DELETE" -H "Content-Type:application/json" localhost:8000/result/:id

        :param: request
        :param: pk: primary key, référence l'id d'une recherche
        :param: format
        :return: Response
        """
        deleted, row_count = SearchModel.objects.get(id=pk).delete()
        return Response({"deleted": row_count})
