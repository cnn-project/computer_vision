from rest_framework import serializers

from plankton.models import SearchModel


class SearchSerializer(serializers.ModelSerializer):
    class Meta:
        model = SearchModel
        fields = ("images",)
