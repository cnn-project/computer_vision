from django.apps import AppConfig


class PlanktonConfig(AppConfig):
    name = 'plankton'
