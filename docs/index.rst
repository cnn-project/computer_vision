Plankton's documentation!
=========================

.. toctree::
   :maxdepth: 4
   :caption: Sommaire:

   setup
   model
   api

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
