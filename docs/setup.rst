Installation du projet
======================

La première étape consiste à copier le dépôt à l'aide de la commande suivante :

.. code-block:: console

    $ git clone https://gitlab.com/cnn-project/computer_vision.git

Environnement de développement
------------------------------

Dans cette section nous allons voir comment configurer et démarrer l'API Rest Django sur une machine de développement

Installation des dépendances
____________________________

Les gestions des dépendances se réalise à l'aide du paquet ``pipenv``, ce dernier permet de faire interface avec un environnement virtuel ``virtualenv``.
L'installation de l'environnement virtuel et des dépendences nécessaires au bon fonctionnement du projet passe par l'appel de la commande ``pipenv install`` comme le montre l'exemple ci-dessous :

.. code-block:: console

    $ python --version
    Python 3.7.5
    $ pip install pipenv
    ...
    $ pipenv install
    ...

Nous noterons par ailleurs que nous utilisons la version ``3.7.5`` de python pour faire tourner l'application.

Migrer la base de données
_________________________

Pour mettre en oeuvre la base de données, il faut définir les tables SQL que les modèles que nous avons définis utiliserons.
Django gère la création de table SQL de manière autonome depuis ce que nous avons dans le fichier ``models.py`` :

.. code-block:: console

    $ pipenv run manage.py makemigrations plankton # Prepare SQL setup (we use sqlite3)
    $ pipenv run manage.py migrate # Apply migrations

Les commandes ci-dessus permettent donc de créer un fichier sqlite (bdd utilisée par défaut) et d'y instancier les tables.

Lancer Django sur sa machine locale
___________________________________

Toujours en utilisant l'outil ``pipenv``, nous allons exécuter une commande dans l'environnement virtuel à l'aide de l'argument ``run``

.. code-block:: console

    $ pipenv run manage.py runserver # Start server on interface 127.0.0.1:8000 by default


Tester le bon fonctionnement de l'API
_____________________________________

Une fois notre application fonctionnelle, nous pouvons effectuer des requètes API à notre application.
Dans l'exemple ci-dessous, nous utilisons l'outil ``curl`` pour envoyer des données à l'application.

.. admonition:: Notez que l'url ``plankton:8000`` fait référence à votre API, dans un environnement de développement nous utiliserons alors ``localhost:8000``.

.. code-block:: console

    $ curl -X POST -H "Content-Type:application/json" -d @tests/skirt.json plankton:8000/search/
    {"id": 1, "url": "http://plankton:8000/results/1/", "status": 200}
    $ curl -X POST -H "Content-Type:application/json" -d @tests/sarko.json plankton:8000/search/
    {"id": 2, "url": "http://plankton:8000/results/2/", "status": 200}
    $ curl -X POST -H "Content-Type:application/json" -d @tests/soup.json plankton:8000/search/
    {"id": 3, "url": "http://plankton:8000/results/3/", "status": 200}
    $ curl -X POST -H "Content-Type:application/json" -d @tests/cowboy.json plankton:8000/search/
    {"id": 4, "url": "http://plankton:8000/results/4/", "status": 200}
    $ curl -H "Content-Type:application/json" plankton:8000/results/
    ["http://plankton:8000/results/1/","http://plankton:8000/results/2/","http://plankton:8000/results/3/","http://plankton:8000/results/4/"]

Dans la dernière requête, nous utilisons l'url ``plankton:8000/results/``, celle-ci nous renvois une liste des recherches effectuées sous forme d'url pour accéder aux ressources.

Environnement de production
---------------------------

L'application est hébergée sur un environnement Kubernetes (nous utilisons Google Kubernetes Engine aka GKE) sous forme de container.
Pour produire une image de container, nous utilisons Docker.

Construire une image docker
___________________________

Afin de constuire une image Docker, nous avons défini dans un fichier Dockerfile le contexte dans lequel doit s'exécuter l'application.
La commande ci-dessous permet ensuite à partir du fichier Dockerfile, de construire une image :

.. code-block:: shell-session

    $ docker build -t plankton:dev .


Pousser l'image sur un registre
_______________________________

Dans cette section, nous allons pousser notre image de docker récemment construite vers le registre gitlab pour faciliter le déploiement ultérieur.

Nous devons d'abord nous connecter au registre des dockers de Gitlab avec nos identifiants gitlab :

.. code-block:: console

    $ docker login registry.gitlab.com

Une fois que nous sommes connectés au registre, nous devons marquer notre image puis l'envoyer avec les commandes suivante :

.. code-block:: console

    $ docker tag plankton:dev registry.gitlab.com/cnn-project/computer_vision:latest
    $ docker push registry.gitlab.com/cnn-project/computer_vision:latest