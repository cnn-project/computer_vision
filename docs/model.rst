Entrainer un réseau de neurones par "transfert learning"
========================================================

Dans cette section, nous allons concevoir un ConvNet en utilisant ``tensoflow``, notre modèle va être entrainé avec `Deepfashion Dataset <https://github.com/aryapei/In-shop-Clothes-From-Deepfashion>`_.

Extraire des caractéristiques d'un réseau de neurones
-----------------------------------------------------

Dans ce projet, nous utiliserons ``InceptionResnetv2`` comme ConvNet avec transfert d'apprentissage pour reconnaître les vêtements.

.. autoclass:: training_model.train_ConvNet.ConvNet
    :members:

Télécharger la source de données
________________________________

Afin d'entraîner notre modèle, nous devons configurer nos données avec les commandes suivantes :

.. code-block:: console

    $ git clone https://github.com/aryapei/In-shop-Clothes-From-Deepfashion.git

Nous allons extraire les caractéristiques d'un ensemble de données donné avec le ConvNet ``InceptionResnetV2`` :

.. literalinclude:: ../training_model/train_ConvNet.py
    :language: python
    :lines: 52-58

Dans l'extrait de code ci-dessus, nous utilisons le procédé suivant :

1. La boucle ``for`` viens itérer sur chaque image dans la base
1. Pour chaque image, nous extrayons une caractéristique normalisée ``norm_feat``
1. Chaque couple image, caractéristique est sauvegardé dans deux tableaux ``names`` et ``feats``

Tester le modèle
""""""""""""""""

.. literalinclude:: ../training_model/test_ConvNet.py
    :language: python
