Documentation python
====================

.. automodule:: plankton
    :members:


Views
"""""

.. autoclass:: plankton.views.SearchRequestViewSet
    :members:

.. autoclass:: plankton.views.ResultsViewSet
    :members:

.. autoclass:: plankton.views.ResultViewSet
    :members:


Models
""""""

.. autoclass:: plankton.models.ImageModel
    :members:


.. autoclass:: plankton.models.SearchModel
    :members:


.. autoclass:: plankton.models.SearchResultModel
    :members:

