import numpy as np
from numpy import linalg as LA
import h5py
from tensorflow.keras.applications import InceptionResNetV2
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.inception_resnet_v2 import preprocess_input
import matplotlib.pyplot as plt
import matplotlib.image as mpimg

img_side_size = 299


class ConvNet:
    def __init__(self):
        self.model = InceptionResNetV2(input_shape=(img_side_size, img_side_size, 3), weights="imagenet",
                                       include_top=False, pooling="max")
        self.model.predict(np.zeros((1, img_side_size, img_side_size, 3)))

    '''
    Use inceptionv3 model to extract features
    Output normalized feature vector
    '''

    def extract_feat(self, img_path):
        img = image.load_img(img_path, target_size=(img_side_size, img_side_size))
        img = image.img_to_array(img)
        img = np.expand_dims(img, axis=0)
        img = preprocess_input(img)
        feat = self.model.predict(img)
        norm_feat = feat[0] / LA.norm(feat[0])
        return norm_feat


# Read the produced files :

h5f = h5py.File('./featureCNN.h5', 'r')
feats = h5f['dataset_feat'][:]
imgNames = h5f['dataset_name'][:]
h5f.close()

print(f"{' searching starts ':=^120}")
queryDir = '/content/In-shop-Clothes-From-Deepfashion/Img/WOMEN/Sweaters/id_00000062/01_1_front.jpg'
queryImg = mpimg.imread(queryDir)

plt.figure()
plt.subplot(2, 1, 1)
plt.imshow(queryImg)
plt.title("Query Image")
plt.axis('off')

model = ConvNet()
queryVec = model.extract_feat(queryDir)
scores = np.dot(queryVec, feats.T)
rank_ID = np.argsort(scores)[::-1]
rank_score = scores[rank_ID]

# number of top retrieved images to show
maxres = 10
local = "/content/In-shop-Clothes-From-Deepfashion/"
distant = "https://raw.githubusercontent.com/aryapei/In-shop-Clothes-From-Deepfashion/master/"
imlist = [f"{local}{imgNames[index].decode('utf-8')}" for i, index in enumerate(rank_ID[0:maxres])]
print("top %d images in order are: " % maxres, imlist)

plt.imshow(queryImg)
plt.title("search input")
plt.axis('off')
plt.show()

for i, im in enumerate(imlist):
    image = mpimg.imread(im)
    plt.imshow(image)
    plt.title("search output %d" % (i + 1))
    plt.axis('off')
    plt.show()
