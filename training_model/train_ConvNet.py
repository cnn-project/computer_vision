#!/usr/bin/env python3

import numpy as np
from numpy import linalg as LA
import h5py
from tensorflow.keras.applications import InceptionResNetV2
from tensorflow.keras.preprocessing import image
from tensorflow.keras.applications.inception_resnet_v2 import preprocess_input
from glob import glob

img_side_size = 299


class ConvNet:
    """
    Cette classe nous permet d'extraire et de normaliser les caractéristiques provenant de notre réseau neuronal ``InceptionResNetV2``.
    """

    def __init__(self):
        self.model = InceptionResNetV2(input_shape=(img_side_size, img_side_size, 3), weights="imagenet",
                                       include_top=False, pooling="max")
        self.model.predict(np.zeros((1, img_side_size, img_side_size, 3)))

    def extract_feat(self, img_path):
        """
        Fonction d'extraction de caractéristiques.

        :param: ``img_path``, prends en entrée le chemin d'une image sur le disque
        :return: ``norm_feat``, vecteur normalisé par la norme euclidienne à partir d'un produit scalaire ou produit hermitien canonique de l'image
        """
        img = image.load_img(img_path, target_size=(img_side_size, img_side_size))
        img = image.img_to_array(img)
        img = np.expand_dims(img, axis=0)
        img = preprocess_input(img)
        feat = self.model.predict(img)
        # LA.norm calculate Frobenius Norm or Euclidean norm by default
        norm_feat = feat[0] / LA.norm(feat[0])
        return norm_feat


if __name__ == "__main__":

    img_dir = "/content/In-shop-Clothes-From-Deepfashion/Img/WOMEN"
    img_pattern = f"{img_dir}/**/**/*.jpg"
    print(img_pattern)
    img_list = glob(img_pattern)

    print(f"{' feature extraction starts ':=^120}")

    feats = []
    names = []

    model = ConvNet()
    img_list_len = len(img_list)
    for i, img_path in enumerate(img_list):
        norm_feat = model.extract_feat(img_path)
        feats.append(norm_feat)
        img_name = '/'.join(img_path.split('/')[-5:])
        names.append(img_name)
        print(f"({i}/{img_list_len}) feat extraction of {img_name}.")

    feats = np.array(feats)
    names = np.string_(names)

    print(f"{' writing feature extraction results ':=^120}")
    h5f = h5py.File("featureCNN.h5", 'w')
    h5f.create_dataset('dataset_feat', data=feats)
    h5f.create_dataset('dataset_name', data=names)
    h5f.close()
