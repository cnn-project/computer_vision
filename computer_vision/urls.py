"""computer_vision URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
# from django.contrib import admin
from django.conf.urls import url
from plankton.views import SearchRequestViewSet, ResultsViewSet, ResultViewSet

urlpatterns = [
    url(r"^search/$", SearchRequestViewSet.as_view()),
    url(r"^results/$", ResultsViewSet.as_view()),
    url(r"^results/(?P<pk>[0-9]+)/$", ResultViewSet.as_view(), name="result"),
]
